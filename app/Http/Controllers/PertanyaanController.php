<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function post(Request $request){
        $pertanyaan = new Pertanyaan;
        
        $pertanyaan->users_id = Auth::id();
        $pertanyaan->nama_user = Auth::user()->name;
        $pertanyaan->isi_pertanyaan = $request->postquestion;
        $pertanyaan->created_at;

        $pertanyaan->save();
        return redirect('/');
    }

    public function beranda(){
        $pertanyaan = Pertanyaan::all(); 
        
        return view('halaman.index', compact('pertanyaan'));
    }

    public function destroy($pertanyaan_id){
        $pertanyaan = Pertanyaan::find($pertanyaan_id);
 
        $pertanyaan->delete();

        return redirect('/');
    }
}