<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Jawaban;

class JawabController extends Controller
{
    public function komen(Request $request){
        $jawaban = new Jawaban();

        $jawaban->nama_responder = Auth::user()->name;
        $jawaban->users_id = Auth::id();
        $jawaban->isi_jawaban = $request->jawaban;
        $jawaban->pertanyaan_id = $request->question_id;
        $jawaban->save();
        return redirect('/');
    }
}