<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['nama','umur','pekerjaan','biodata','alamat','users_id'];

    public function user(){
        return $this->belongsTo('App\User');
      }
}