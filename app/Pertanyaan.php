<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['nama_user','isi_pertanyaan','total_jawaban','kategori_id','users_id'];
    
    public function user(){
        return $this->belongsTo('App\User');
      }
      public function jawaban(){
        return $this->hasMany('App\Jawaban');
      }
}