<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawaban';
    protected $fillable = ['nama_responder','isi_jawaban','pertanyaan_id','users_id'];

    public function user(){
        return $this->belongsTo('App\User');
      }
    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan');
    }
}