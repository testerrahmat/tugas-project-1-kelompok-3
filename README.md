## Final Project

## Kelompok 3

## Anggota Kelompok

-   Muhammad Hanif Suyuthi
-   Rahmat Hidayat
-   Rahul Ilsa Tajri Mukhti

## Tema Project

Forum Tanya Jawab

## ERD

<img src="public/ERD_Project_1_Kelompok_3.png" width="400">

## Link Video

<p>Link Demo Aplikasi : <a href="https://drive.google.com/file/d/1C7l7OikoeIicRpmdMF94TwiaEYWv0khx/view?usp=sharing" target="_blank">Link Video GDrive</a></p>
<p>Link Deploy : <a href="https://laravel.com" target="_blank">Link Video GDrive</a></p>
