<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/master', function () {
    return view('layout.front');
});



Route::get('/', 'IndexController@Index');
Route::get('/', 'UserController@read'); //READ Data User
Route::get('/', 'PertanyaanController@beranda');
Route::delete('/{users_id}/{pertanyaan_id}', 'PertanyaanController@destroy');

 //Tampil semua pertanyaan

Route::group(['middleware' => ['auth']], function () {
    Route::post('/', 'PertanyaanController@post'); //POST pertanyaan ke DB
    Route::post('/{users_id}/{pertanyaan_id}', 'JawabController@komen');
});

// Route::get('/register', 'authcontroller@bio' );

// Route::get('/master', function(){
//     return view('layout.master');
// });


// //KATEGORI

// //Create
// Route::get('/cast/create','CastController@create');
// Route::post('/cast', 'CastController@store');

// //Read
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/{cast_id}', 'CastController@show');


// //edit&update
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update'); 

// //delete
// Route::delete('/cast/{cast_id}', 'CastController@destroy');



// //PROFILE

// //Create
// Route::get('/cast/create','CastController@create');
// Route::post('/cast', 'CastController@store');

// //Read
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/{cast_id}', 'CastController@show');

// //edit&update
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update'); 

// //delete
// Route::delete('/cast/{cast_id}', 'CastController@destroy');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');