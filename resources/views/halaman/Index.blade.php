@extends('layout.front')

{{-- @section('judul')
Halaman Home
@endsection --}}

@section('content')
    @forelse ($pertanyaan as $key => $tanya)
        <div class="card-deck mt-3">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">{{ $tanya->nama_user }}</h3>
                    <form action="/{{ $tanya->users_id }}/{{ $tanya->id }}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm float-right" value="Delete">
                    </form>
                    <p class="card-text">{{ $tanya->isi_pertanyaan }}</p>

                    <!-- Button Jawab -->
                    <!--<div class="btn-group">
                                                                                    <button class="btn btn-default" id="btn-komentar-utama" onclick="$('#komentar-utama').toggle('slide')" value={{ $tanya->id }}>Jawab</button>
                                                                                    </div>-->
                    <p class="card-text"><small class="text-muted">{{ $tanya->created_at }}</small></p>

                    @auth
                        <!-- Form Jawab -->
                        <form action="/{{ $tanya->users_id }}/{{ $tanya->id }}" method="POST" id="komentar-utama"
                            value="{{ $tanya->id }}">
                            @csrf
                            <input type="hidden" name="question_id" value="{{ $tanya->id }}" id="{{ $tanya->id }}">
                            <textarea name="jawaban" cols="40" rows="1"></textarea>
                            <input type="submit" class="btn btn-primary radius btn-sm mb-3" value="Jawab">
                        </form>
                    @endauth
                    @foreach ($tanya->jawaban as $item)
                        <p class="card-text">{{ $item->nama_responder }} : {{ $item->isi_jawaban }} <small
                                class="text-muted">({{ $item->created_at }})</small></p>
                    @endforeach
                </div>
            </div><br>
        </div><br>
    @empty
        <h1>Tidak ada yg nanya</h1>
    @endforelse
    <br>
@endsection
