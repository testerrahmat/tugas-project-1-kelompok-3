<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kuskas</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('admin/assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/assets/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/animate.css/animate.css" type="text/css" />
    <link rel="stylesheet" href="https://unpkg.com/rmodal/dist/rmodal.css" type="text/css" />
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End Plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{ asset('admin/assets/css/style.css') }}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{ asset('admin/assets/images/favicon.png') }}" />
</head>

<body>
    <div class="container-scroller">
        <!-- partial:../../partials/_sidebar.html -->
        {{-- @include('partial.sidebar') --}}


        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:../../partials/_navbar.html -->
            @include('partial.nav')



            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">

                    <div class="container">


                        {{-- @include('layouts.partials.error')
          
              @include('layouts.partials.success') --}}

                        <div class="row">

                            <div class="col-3">
                                <h2> Forum</h2>

                                @section('category')
                                    @include('partial.categories')
                                @show
                            </div>

                            <div class="col-6">
                                <div class="col">
                                    <div class="content-wrap ">
                                        @yield('content')
                                    </div>
                                </div>
                            </div>

                            <div class="col-3">

                                @section('search')
                                    @include('partial.search')
                                @show
                            </div>

                        </div>
                    </div>

                </div>
                <!-- content-wrapper ends -->

                <!-- partial:../../partials/_footer.html -->
                @include('partial.footer')
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('admin/assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('admin/assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('admin/assets/js/hoverable-collapse.j') }}s"></script>
    <script src="{{ asset('admin/assets/js/misc.js') }}"></script>
    <script src="{{ asset('admin/assets/js/settings.js') }}"></script>
    <script src="{{ asset('admin/assets/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <!-- End custom js for this page -->
    <script type="text/javascript" src="https://unpkg.com/rmodal/dist/rmodal.js"></script>
    <style type="text/css">
        .modal .modal-dialog {
            width: 400px;
        }

    </style>

    <script type="text/javascript">
        window.onload = function() {
            var modal = new RModal(document.getElementById('modal'), {
                //content: 'Abracadabra'
                beforeOpen: function(next) {
                    console.log('beforeOpen');
                    next();
                },
                afterOpen: function() {
                    console.log('opened');
                },
                beforeClose: function(next) {
                    console.log('beforeClose');
                    next();
                },
                afterClose: function() {
                    console.log('closed');
                },
                dialogOpenClass: 'animate__slideInDown',
                dialogCloseClass: 'animate__slideOutUp'
                // bodyClass: 'modal-open',
                // dialogClass: 'modal-dialog',

                // focus: true,
                // focusElements: ['input.form-control', 'textarea', 'button.btn-primary'],

                // escapeClose: true
            });

            document.addEventListener('keydown', function(ev) {
                modal.keydown(ev);
            }, false);

            document.getElementById('showModal').addEventListener("click", function(ev) {
                ev.preventDefault();
                modal.open();
            }, false);

            window.modal = modal;
        }
    </script>



</body>

</html>
